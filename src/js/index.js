import { eventListenersModule, h, init } from 'snabbdom'

import { dispatch, initState, subscribe } from './state'
import { ActionGroup, Button, Count } from './modules'

const container = document.getElementById('root')

const patch = init([
	eventListenersModule
])

const vnode = state => h('main', [
	h('h1', 'Observable Clicker'),
	Count({ label: 'Click', value: state.clicks }),
	Count({ label: 'Auto-clicker', value: state.autoClickers }),
	Button({ id: 'clicker', click: () => dispatch('click'), label: 'Click' }),
	Button({ click: () => dispatch('reset'), label: 'Reset' }),
	ActionGroup({ action: 'buy-autoclick', base: -10, verb: 'Get' }),
	ActionGroup({ action: 'sell-autoclick', base: 5, verb: 'Sell' })
])

let node = patch(container, vnode(initState))

subscribe(state => {
	node = patch(node, vnode(state))
})
