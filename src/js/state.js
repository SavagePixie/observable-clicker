import { filter, fromEvent, interval, map, merge, mergeScan, of, tap } from 'rxjs'

import { storage } from './utils'

const initState = storage.findOr(
	{ clicks: 0, autoClickers: 0 },
	'state'
)

// Actions
const decrease = (key, value) =>
	state => ({ ...state, [key]: state[key] - value })
const increase = (key, value) =>
	state => ({ ...state, [key]: state[key] + value })
const reset = () => ({ clicks: 0, autoClickers: 0 })
const tick = state =>
	({ ...state, clicks: state.clicks + state.autoClickers })

// Filters
const hasClicks = value => state => state.clicks >= value
const hasAutoClickers = value => state => state.autoClickers >= value

// Observables
const auto$ = fromEvent(window, 'state-buy-autoclick')
	.pipe(filter(event => Number.isFinite(event.detail) && event.detail > 0))
	.pipe(map(event => event.detail))
	.pipe(map(qty => ({
		actions: [increase('autoClickers', qty), decrease('clicks', 10 * qty)],
		filters: [hasClicks(10 * qty)]
	})))

const click$ = fromEvent(window, 'state-click')
	.pipe(map(() => ({ actions: [increase('clicks', 1)] })))

const reset$ = fromEvent(window, 'state-reset')
	.pipe(map(() => ({ actions: [reset] })))

const sell$ = fromEvent(window, 'state-sell-autoclick')
	.pipe(filter(event => Number.isFinite(event.detail) && event.detail > 0))
	.pipe(map(event => event.detail))
	.pipe(map(qty => ({
		actions: [decrease('autoClickers', qty), increase('clicks', 5 * qty)],
		filters: [hasAutoClickers(qty)]
	})))

const timer$ = interval(5_000)
	.pipe(map(() => ({ actions: [tick] })))

const reduceHandlers = (state, { actions = [], filters = [] } = {}) =>
	of(
		filters.every(pred => pred(state))
			? actions.reduce((acc, action) => action(acc), state)
			: state
	)

// Exports
const dispatch = (event, detail) =>
	window.dispatchEvent(new CustomEvent(`state-${event}`, { detail }))

const subscribe = patch => merge(auto$, click$, reset$, sell$, timer$)
	.pipe(mergeScan(reduceHandlers, initState))
	.pipe(tap(storage.store('state')))
	.subscribe(patch)

export {
	dispatch,
	initState,
	subscribe
}
