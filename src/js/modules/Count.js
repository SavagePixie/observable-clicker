import { h } from 'snabbdom'

const Count = ({ label, value }) =>
	h('p', `${label} count: ${value.toLocaleString()}`)

export {
	Count
}
