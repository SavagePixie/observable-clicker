import { h } from 'snabbdom'

const Button = ({ id, click, label }) => {
	const tag = id ? `button#${id}` : 'button'
	return h(tag, { on: { click } }, label)
}

export {
	Button
}
