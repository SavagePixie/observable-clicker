import { h } from 'snabbdom'

import { dispatch } from '../state'
import { Button } from './Button'

const ActionGroup = ({ action, base, verb }) =>
	h('div', [
		Button({
			click: () => dispatch(action, 1),
			label: `${verb} an auto-clicker (${base} clicks)`
		}),
		Button({
			click: () => dispatch(action, 10),
			label: `${verb} 10 auto-clickers (${10 * base} clicks)`
		}),
		Button({
			click: () => dispatch(action, 100),
			label: `${verb} 100 auto-clickers (${100 * base} clicks)`
		})
	])

export {
	ActionGroup
}
