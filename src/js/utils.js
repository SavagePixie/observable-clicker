const findOr = (def, key) => {
	const value = window.localStorage.getItem(key)
	return value
		? JSON.parse(value)
		: def
}

const store = key =>
	value => window.localStorage.setItem(key, JSON.stringify(value))

const storage = { findOr, store }

export {
	storage
}
